# Contributing Guidelines

Some basic conventions for contributing to this project.

### General

Please make sure that there aren't existing pull requests attempting to address the issue mentioned. Likewise, please check for issues related to update, as someone else may be working on the issue in a branch or fork.

* Non-trivial changes should be discussed in an issue first
* Develop in a topic branch, not main
* Squash your commits

### Commit Message Format

Each commit message should include a **type**, a **scope** and a **subject**:

```
 <type>(<scope>): <subject>
```

Lines should not exceed 100 characters. This allows the message to be easier to read on github as well as in various git tools and produces a nice, neat commit log ie:

```
 #271 new(linux): add SELinux commands
 #270 fix(freebsd): fix iocage snapshot rollback command
 #269 new(backups): add borg diff commands
 #268 sort(hacking): move `webcam` to media
``` 

#### Type

Must be one of the following:

* **new**: A new command
* **fix**: A bug fix
* **typo**: A typo fix
* **docs**: Documentation only changes
* **sort**: Arrange command(s) / Moving command(s) to better group
* **style**: Changes that do not affect the commands themselves (white-space, formatting, missing
  semi-colons, etc)
* **refactor**: A code change that neither fixes a bug or adds a command

#### Scope

The scope should refere to the main category the commit belongs to. For example `MacOSX`,
`Hacking`, `Version Control` ..etc.

#### Subject

The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end


---------------------------------------------------------------------------------------


Source: https://github.com/davezuko/react-redux-starter-kit/blob/master/CONTRIBUTING.md
