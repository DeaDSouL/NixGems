# NixGems

[![NixGems](https://gitlab.com/DeaDSouL/NixGems/-/raw/main/NixGems.png)](https://gitlab.com/DeaDSouL/NixGems/-/blob/main/NixGems.png)


## What is NixGems?

It is a useful sets of commands that are being used in #UNIX, #Linux, #MacOSX, #FreeBSD,
and some other tools like #GPG, #Find, #SSH, ..etc. As it also has some brief information
and tweaks about some tools and how-to(s). All that are written in a good organized
cheatsheet style.


## Why does NixGems exist?

It was meant for me, as a mini-handbook. To save me some time spent on `DuckDuckGo`.
But since I find it really useful, specially the amount of information it has, I decided
to share it with everyone.


## Some commands are really old, Why?

AFAIR, The first time I wrote to it, was about 18 years ago
So, if you find an obsolete commad, please update it, then
share it back with me to make it available for everyone else.


## Requirements?

- [ ] `VIM`: Since I used the `Marker` fold-method in it.
  * To enable it make sure you have `set modeline` in your `.vimrc`. Otherwise you'd have to enable it every single time you edit/view the file by typing `:set foldmethod=marker`.
  * You can use your favorit editor as long as it supports the `Marker` folding. Which is `{{{` ...text... `}}}`. Otherwise, the file will look like a huge mess.


## Installation
Just clone the repository. Then open `NixGems.txt`.


## Usage: How to work with `VIM` folding?

If you're new to `VIM`.. Just use `za`.

* Create fold: visual select what you want, then hit: `zf`.
* Delete folde: go to folded line then hit: `zd`.
* `zo`: open fold.
* `zc`: close fold.
* `za`: toggle fold.
* `zO`, zC, zA: same as above, but for all.
* `zr`: open all folds.
* `zm`: close all folds.


## Contributing?
I'm very open for contributions. If you think you know a good command, or there's missing
in `NixGems`, or there is something obsolete, and you'd like to contribute, please fork
the repository, commit your changes, and request for a merge.

Please don't forget to take a look on [CONTRIBUTING.md](https://gitlab.com/DeaDSouL/NixGems/-/blob/main/CONTRIBUTING.md)

Your contribution is highly appreciated!


## License?
[GNU GENERAL PUBLIC LICENSE Version 3 (GPL v3)](https://gitlab.com/DeaDSouL/NixGems/-/blob/main/LICENSE).


## Project status?
Up and running!
